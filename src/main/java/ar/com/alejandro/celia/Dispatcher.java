package ar.com.alejandro.celia;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ar.com.alejandro.celia.model.Director;
import ar.com.alejandro.celia.model.Empleado;
import ar.com.alejandro.celia.model.EmpleadoState;
import ar.com.alejandro.celia.model.Llamada;
import ar.com.alejandro.celia.model.Operador;
import ar.com.alejandro.celia.model.Supervisor;

public class Dispatcher {

	/*
	 * Logueo de la aplicacion
	 */
	private static final Logger logger = LogManager.getLogger(Dispatcher.class);

	/*
	 * Lista de empelados disponibles. 
	 */
	private ConcurrentLinkedDeque<Empleado> empleados = new ConcurrentLinkedDeque<Empleado>();

	/*
	 * Lista de llamadas atendidas correctamente.
	 */
	public static ConcurrentLinkedQueue<Llamada> llamadasAtendidas;

	/*
	 *  Hilos a ejecutar concurrentes.
	 *  
	 *  Esto da una solucion al punto extra 2. 
	 *  Cuando lleguen mas de 10 llamadas, el proceso va a ejecutar solo los 10 hilos que especifique 
	 *  y va a dejar encolada la siguiente ejecucion, hasta que un hilo se libere.
	 */
	private ExecutorService executor = Executors.newFixedThreadPool(10);

	/**
	 * Constructor de Dispatcher.
	 * 
	 * @param empleados Lista de empleados a cargar inicialmente. 
	 */
	public Dispatcher(List<Empleado> empleados) {
		logger.info("Nueva instancia de Dispatcher");

		// Cargo los empleados, controlando que no este nula la lista
		if (empleados != null) {
			this.empleados.addAll(empleados);
		}

		// Inicializo la lista de llamadas atendidas
		llamadasAtendidas = new ConcurrentLinkedQueue<>();
	}

	/**
	 * Busco un empleado que pueda tomar la llamada y se la asigno.
	 * 
	 * @param llamada Llamada recibida.
	 */
	private synchronized void dispatchCall(Llamada llamada) {
		logger.info("Se va a asignar la llamada: " + llamada.toString());

		// Busco un empleado disponible
		Empleado empleado = this.buscarEmpleadoDisponiblePorTipo();
		// El empleado esta ocupado con la llamada
		empleado.setEstadoEmpleado(EmpleadoState.ATENDIENDO_LLAMADA);

		// Creo una nueva taera con el empleado seleccionado
		Runnable task = () -> {
			// Le asigno la llamada a un empleado
			empleado.atenderLlamada(llamada);
		};

		// Ejecuto la llamada en otro hilo
		Thread thread = new Thread(task);
		thread.start();
	}

	/**
	 * Busco los empleados disponibles.
	 * De los empleados disponibles, busco uno que sea operador, sino supervisor, y sino director.
	 * 
	 * @return Empleado disponible segun criterio, para atender una llamada.
	 */
	private Empleado buscarEmpleadoDisponiblePorTipo() {
		// Filtro solo a los emmpleados disponibles
		List<Empleado> empleadosDisponibles = this.empleados.stream().filter(e -> e.getEstadoEmpleado() == EmpleadoState.DISPONIBLE).collect(Collectors.toList());
		
		// Mientras no haya empleado disponible, pongo el "contestador" y vuelvo a buscar
		// Esto da una solucion al primer punto extra
		while(empleadosDisponibles.size() == 0) {

			try {
				// Espero 2 segundos, podria ser un msj al estilo contestador
				logger.info("Todos nuestros operadores estan ocupados, por favor aguarde");
				TimeUnit.SECONDS.sleep(2);

			} catch (InterruptedException e) {
				logger.error("Fallo el 'contestador'");
				e.printStackTrace();
			}

			//  Filtro solo a los emmpleados disponibles
			empleadosDisponibles = this.empleados.stream().filter(e -> e.getEstadoEmpleado() == EmpleadoState.DISPONIBLE).collect(Collectors.toList());
		}

		// Busco el empleado a atender, por tipo
		return this.buscarEmpleadoPorTipo(empleadosDisponibles);
	}

	/**
	 * De los empleados disponibles, busco uno que sea operador, sino supervisor, y sino director.
	 * 
	 * @param empleadosDisponibles lista de empleados con estado disponible
	 * @return empleado disponible segun criterio, para atender una llamada.
	 */
	private Empleado buscarEmpleadoPorTipo(List<Empleado> empleadosDisponibles) {
		// Busco algun operador
		Optional<Empleado> empleado = empleadosDisponibles.stream().filter(e -> e instanceof Operador).findAny();

		// Si encuentro al menos un operador sigo
		if (!empleado.isPresent()) {
			// Si no habia operadores, busco supervisores
			empleado = empleadosDisponibles.stream().filter(e -> e instanceof Supervisor).findAny();

			// Si encuentro al menos un supervisor sigo
			if (!empleado.isPresent()) {
				// Si no habia operadores, ni supervisores, busco directores
				empleado = empleadosDisponibles.stream().filter(e -> e instanceof Director).findAny();
			}
		}

		// Devuelvo el empleado encontrado
		return empleado.get();
	}

	/**
	 * Recibo hasta 10 llamadas para procesar en paralelo
	 * 
	 * @param llamadas
	 */
	public void procesar10LlamadasConcurrentes(List<Llamada> llamadas) {
		// Recorro cada una de las llamadas
		llamadas.stream().forEach(llamada -> {
			// Armo un hilo por cada llamada (hasta 10)
			this.executor.submit(() -> {
				this.dispatchCall(llamada);
			});
        });
	}
}

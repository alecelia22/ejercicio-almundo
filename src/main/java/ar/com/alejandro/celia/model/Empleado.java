package ar.com.alejandro.celia.model;

import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ar.com.alejandro.celia.Dispatcher;

public abstract class Empleado {

	/*
	 * Logueo de la aplicacion
	 */
	private static final Logger logger = LogManager.getLogger(Empleado.class);

	/*
	 * Determino si el empleado esta disponible o atendiendo una llamada.
	 * Por default, esta disponible.
	 */
	private EmpleadoState estadoEmpleado = EmpleadoState.DISPONIBLE;

	/**
	 * El empleado atiende la llamada.
	 * Hago un sleep que representa la duracion de la llamada.
	 * 
	 * @param llamada Llamada que tiene que atender el empleado
	 */
	public void atenderLlamada(Llamada llamada) {
		logger.info("Empleado " + this.toString() + " atendiendo la llamada " + llamada.toString());

		try {
			// Tiempo que dura la llamada
			TimeUnit.SECONDS.sleep(llamada.duracion);

		} catch (InterruptedException e) {
			// Si falla la llamada, solo logueo el error
			logger.error("Fallo la llamada: " + llamada.toString() + " atendida por el empleado: " + this.toString());
			e.printStackTrace();
		}

		logger.info("Empleado " + this.toString() + " finalizo la llamada");

		// El empleado termino la llamada
		this.estadoEmpleado = EmpleadoState.DISPONIBLE;

		// Cargo la llamada a la lista de llamadas atendidas
		Dispatcher.llamadasAtendidas.add(llamada);
	}

	/**
	 * Get del estado del empleado.
	 * Lo hago sincronico, porque lo pueden pedir varios hilos a la vez.
	 * 
	 * @return estado del empleado
	 */
	public synchronized EmpleadoState getEstadoEmpleado() {
		return estadoEmpleado;
	}

	/**
	 * Set del estado del empleado.
	 * Lo hago sincronico, porque lo pueden pedir varios hilos a la vez.
	 * 
	 * @param estadoEmpleado nuevo estado del empleado
	 */
	public synchronized void setEstadoEmpleado(EmpleadoState estadoEmpleado) {
		this.estadoEmpleado = estadoEmpleado;
	}
}

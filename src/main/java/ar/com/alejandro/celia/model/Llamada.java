package ar.com.alejandro.celia.model;

import java.util.concurrent.ThreadLocalRandom;


/**
 * Llamada telefonica.
 *
 */
public class Llamada {

	/* 
	 * Duracion de la llamada, en segundos.
	 * Lo inicializo con un numero random entre 5 y 10 como pide el ejercicio.
	 */
	public Integer duracion = ThreadLocalRandom.current().nextInt(5, 10 + 1);

}

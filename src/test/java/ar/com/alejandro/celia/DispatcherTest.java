package ar.com.alejandro.celia;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import ar.com.alejandro.celia.model.Director;
import ar.com.alejandro.celia.model.Empleado;
import ar.com.alejandro.celia.model.Llamada;
import ar.com.alejandro.celia.model.Operador;
import ar.com.alejandro.celia.model.Supervisor;
import junit.framework.TestCase;

public class DispatcherTest extends TestCase {
	
	/*
	 * Logueo del test
	 */
	private static final Logger logger = LogManager.getLogger(DispatcherTest.class);

	@Test(expected = NullPointerException.class)
    public void testInicializarDispatcherSinEmpleados() {
		logger.info("Comenzando test testInicializarDispatcherSinEmpleados");

        new Dispatcher(null);
    }

	/**
	 * Test basico para validar el procesar 10 llamadas. 
	 * 
	 * @throws InterruptedException
	 */
	@Test
	public void testProcesar10LlamadasConcurrentes() throws InterruptedException {
		logger.info("Comenzando test testProcesar10LlamadasConcurrentes");

		// Cantidad de llamadas a relizar
		int cantidadLlamadas = 10;

		// Cargo la lista de empleados
		List<Empleado> empleados = this.cargarEmpleados(5, 3, 2);
		
		// Cargo una lista de llamadas
		List<Llamada> llamadas = new ArrayList<Llamada>();

		// Cargo 10 llamadas
		IntStream.range(0, cantidadLlamadas).forEach(
			i -> llamadas.add(new Llamada())
		);

		// Inicializo el dispatcher con sus empleados
		Dispatcher dispatcher = new Dispatcher(empleados);
		// le paso 10 llamadas para que las procese
		dispatcher.procesar10LlamadasConcurrentes(llamadas);

		// Espero un poco mas de 10 segundos, que puede ser la llamada mas larga
		TimeUnit.SECONDS.sleep(15);

		// Me fijo que se atendieran 10 llamadas
		assertEquals(cantidadLlamadas, Dispatcher.llamadasAtendidas.size());

		logger.info("Finalizo el test testProcesar10LlamadasConcurrentes");
	}

	/**
	 * Pruebo de procesar 10 llamadas con solo 6 empleados.
	 * Esto va a forzar que en algun momento, no haya ningun empleado disponible.
	 * 
	 * @throws InterruptedException
	 */
	@Test
	public void testProcesar10LlamadasConcurrentes2() throws InterruptedException {
		logger.info("Comenzando test testProcesar10LlamadasConcurrentes2");

		// Cantidad de llamadas a relizar
		int cantidadLlamadas = 10;

		// Cargo la lista de empleados
		List<Empleado> empleados = this.cargarEmpleados(3, 2, 1);
		
		// Cargo una lista de llamadas
		List<Llamada> llamadas = new ArrayList<Llamada>();

		// Cargo 10 llamadas
		IntStream.range(0, cantidadLlamadas).forEach(
			i -> llamadas.add(new Llamada())
		);

		// Inicializo el dispatcher con sus empleados
		Dispatcher dispatcher = new Dispatcher(empleados);
		// le paso 10 llamadas para que las procese
		dispatcher.procesar10LlamadasConcurrentes(llamadas);

		// Espero el doble de 10 segundos, porque tengo menos gente para atender
		TimeUnit.SECONDS.sleep(20);

		// Me fijo que se atendieran 10 llamadas
		assertEquals(cantidadLlamadas, Dispatcher.llamadasAtendidas.size());

		logger.info("Finalizo el test testProcesar10LlamadasConcurrentes2");
	}

	/**
	 * Envio a procesar 30 llamadas con 10 empleados atendiendo
	 * 
	 * @throws InterruptedException
	 */
	@Test
	public void testProcesar30Llamadas() throws InterruptedException {
		logger.info("Comenzando test testProcesar30Llamadas");

		// Cantidad de llamadas a relizar
		int cantidadLlamadas = 30;

		// Cargo la lista de empleados
		List<Empleado> empleados = this.cargarEmpleados(5, 3, 2);

		// Cargo una lista de llamadas
		List<Llamada> llamadas = new ArrayList<Llamada>();

		// Cargo 10 llamadas
		IntStream.range(0, cantidadLlamadas).forEach(
			i -> llamadas.add(new Llamada())
		);

		// Inicializo el dispatcher con sus empleados
		Dispatcher dispatcher = new Dispatcher(empleados);
		// le paso 10 llamadas para que las procese
		dispatcher.procesar10LlamadasConcurrentes(llamadas);

		// Espero un buen tiempo, para que todas las ejecuciones terminen
		TimeUnit.SECONDS.sleep(40);

		// Me fijo que se atendieran 10 llamadas
		assertEquals(cantidadLlamadas, Dispatcher.llamadasAtendidas.size());

		logger.info("Finalizo el test testProcesar30Llamadas");
	}

	/**
	 * Creo una lista de empleados, con una cierta cantidad de operadores, supervisores y directores.
	 * 
	 * @param operadores cantidad de operadores a cargar
	 * @param supervisores cantidad de supervisores a cargar
	 * @param directores cantidad de directores a cargar
	 * @return lista de empleados
	 */
	public List<Empleado> cargarEmpleados(Integer operadores, Integer supervisores, Integer directores) {
		// Inicializo los empleados
		List<Empleado> empleados = new ArrayList<Empleado>();

		// Cargo Operadores a la lista e empleados
		IntStream.range(0, operadores).forEach(
			i -> empleados.add(new Operador())
		);

		// Cargo Supervisores a la lista e empleados
		IntStream.range(0, supervisores).forEach(
			i -> empleados.add(new Supervisor())
		);

		// Cargo Directores a la lista e empleados
		IntStream.range(0, directores).forEach(
			i -> empleados.add(new Director())
		);

		return empleados;
	}
}
